<?php

//Permet de charger les différentes classes (Script d'autoload)
include 'autoload.php';

// Appel la page qui contient l'ensemble des paramètre pour se connecter à la base de donnée
include 'bd/config.php';

// Appel de la base de donnée
include 'bd/connexion.php';

include 'header.php';

// Connexion à la base de données
$db = connect($config);


// Si la base de données renvoie une valeur différente de NULL
if ($db != NULL) {
    $userManager = new UserManager($db);


    if (isset($_POST['creer'])) {
        if (!empty($_POST['email']) && !empty($_POST['pseudo']) && !empty($_POST['mdp'])) {
            $array_user = array(
                'email' => $_POST['email'],
                'pseudo' => $_POST['pseudo'],
                'mdp' => $_POST['mdp'],
                'admin' => 1,
                'dateInscription' => date('Y-m-d'),
                'actif' => 0
            );

            $user = new User($array_user);
            $userManager->add($user);
        }
    }


    ?>
    <div class="container">
        <h2>Rover Photos</h2>
        <form method="post" action="">
            <div class="form-group">
                <label for="email">Email :</label>
                <input class="form-control" type="email" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="pseudo">Pseudo :</label>
                <input class="form-control" type="pseudo" name="pseudo" id="pseudo">
            </div>
            <div class="form-group">
                <label for="mdp">Mot de passe :</label>
                <input class="form-control" type="mdp" name="mdp" id="mdp">
            </div>
            <input class="btn btn-primary" type="submit" name="creer" value="Créer l'utilisateur">
        </form>
    </div>
    <?php
}



// Sinon nous indiquons à l'utilisateur que le site est indisponible
    else {

        echo'<h1>ERREUR BDD</h1>';
    }



/*

$array = array(
    'id' => '11',
    'email' => 'louison.chevalier@gmail.com',
    'pseudo' => 'LouisonLeCheval',
    'mdp' => 'motdepasse',
    'admin' => '',
    'date_inscription' => '',
    'actif' => '1'
);

$user = new User($array);

echo '<pre>';
print_r($user);
echo '</pre>';
*/
include 'footer.php';

?>