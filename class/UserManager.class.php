<?php

class UserManager{

    private $bd;

    /**
     * UserManager constructor.
     * @param $bd
     */
    public function __construct($bd)
    {
        $this->setBd($bd);
    }


    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }


    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * Add User
     */
    public function add(User $user){
        $req = $this->getBd()->prepare("INSERT INTO user (email, pseudo, mdp, admin, date_inscription, actif) VALUES (:email, :pseudo, :mdp, :admin, :dateInscription, :actif);");
        $req->bindValue(':email', $user->getEmail());
        $req->bindValue(':pseudo', $user->getPseudo());
        $req->bindValue(':mdp', $user->getMdp());
        $req->bindValue(':admin', $user->getAdmin());
        $req->bindValue(':dateInscription', $user->getDateInscription());
        $req->bindValue(':actif', $user->getActif());
        $req->execute();
        $user->hydrate([
            'id' => $this->getBd()->lastInsertId()
        ]);
    }

    /**
     * Edit User
     */
    public function update(User $user){
        $req = $this->getBd()->prepare('UPDATE user SET email=:email , pseudo=:pseudo, mdp=:mdp, admin=:admin, date_inscription=:date_inscription, actif=:actif WHERE id=:id');
        $req->bindValue(':email', $user->getEmail());
        $req->bindValue(':pseudo', $user->getPseudo());
        $req->bindValue(':mdp', $user->getMdp());
        $req->bindValue(':admin', $user->getAdmin());
        $req->bindValue(':date_inscription', $user->getDateInscription());
        $req->bindValue(':actif', $user->getActif());
        $req->bindValue(':id', $user->getId());
        $req->execute();
    }


    /**
     * Delete User
     */
    public function delete(User $user){
        $req = $this->getBd()->prepare('DELETE FROM user WHERE id=:id');
        $req->bindValue(':id', $user->getId());
        $req->execute();
    }


    /**
     * Count User
     */
    public function count(User $user){
        $req = $this->getBd()->prepare('SELECT count * FROM user');
        $req->execute();
    }
    //


    /**
     * List User
     */
    public function selectAll(User $user){
        $req = $this->getBd()->prepare('SELECT * FROM user');
        $req->execute();
    }


    /**
     * Info User
     */
    public function selectOne(User $user){
        $req = $this->getBd()->prepare('SELECT * FROM user WHERE idtype=:idtype');
        $req->bindValue(':id', $user->getId());
        $req->execute();
    }


    /**
     *  User
     */
    public function present(User $user){
        $req = $this->getBd()->prepare('SELECT idtype=:idtype FROM user');
        $req->bindValue(':id', $user->getId());
        $req->execute();
    }
}

?>