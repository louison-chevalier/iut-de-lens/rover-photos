/*<?php

class Gallerie
{
    private $id;
    private $titre;
    private $descritpion;
    private $dateCreation;
    private $id_user_creation;

    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }

    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getDescritpion()
    {
        return $this->descritpion;
    }

    /**
     * @param mixed $descritpion
     */
    public function setDescritpion($descritpion)
    {
        $this->descritpion = $descritpion;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getIdUserCreation()
    {
        return $this->id_user_creation;
    }

    /**
     * @param mixed $id_user_creation
     */
    public function setIdUserCreation($id_user_creation)
    {
        $this->id_user_creation = $id_user_creation;
    }


}

?>